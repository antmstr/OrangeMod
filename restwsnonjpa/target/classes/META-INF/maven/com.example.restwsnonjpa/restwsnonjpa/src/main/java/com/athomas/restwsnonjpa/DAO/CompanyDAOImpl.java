package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.entities.Company;
import com.athomas.restwsnonjpa.entities.User;

@Repository
public class CompanyDAOImpl implements CompanyDAO {

	@Autowired
	EntityManagerFactory em;

	private SessionFactory factory;

	private void someService() {
		if (em.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("factory is not a hibernate factory");
		}
		factory = em.unwrap(SessionFactory.class);
	}

	@Override
	public List<Company> getCompanies() {
		someService();
		Session session = factory.getCurrentSession();
		List<Company> companies = session.createQuery("from Company").list();
		return companies;
	}

	@Override
	public Company getCompany(Integer companyId) {
		someService();		
		Session session = factory.getCurrentSession();
		Company company = (Company) session.createQuery("from Company where id in ('" + companyId + "')")
				.uniqueResult();
		return company;
	}

}
