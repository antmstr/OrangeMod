package com.athomas.restwsnonjpa.service;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import com.athomas.restwsnonjpa.DAO.CompanyDAO;
import com.athomas.restwsnonjpa.entities.Company;
import com.athomas.restwsnonjpa.entities.Team;

@RequestScope
@Transactional
@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	CompanyDAO companyDAO;

	@Override
	public List<Company> getCompanies() {
		return companyDAO.getCompanies();
	}

	@Override
	public Company getCompany(Integer id) {
		return companyDAO.getCompany(id);
	}

}
