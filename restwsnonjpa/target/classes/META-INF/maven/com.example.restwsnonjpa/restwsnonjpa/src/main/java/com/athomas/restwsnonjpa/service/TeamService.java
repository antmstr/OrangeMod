package com.athomas.restwsnonjpa.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.athomas.restwsnonjpa.entities.Team;

@Transactional
@RestController
public interface TeamService {
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "teamservice/teams", method = RequestMethod.GET, produces = "application/json")
	public List<Team> getTeams();
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "teamservice/team", method = RequestMethod.GET, produces = "application/json")
	public Team getTeam(@RequestParam(required = true) Integer id);

}