package com.athomas.restwsnonjpa.service;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.athomas.restwsnonjpa.entities.Company;


@RestController
public interface CompanyService {
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/companyservice/companies", method = RequestMethod.GET, produces = "application/json")
	public List<Company> getCompanies();
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/companyservice/company", method = RequestMethod.GET, produces = "application/json")
	public Company getCompany(@RequestParam(required = true) Integer id);

}

