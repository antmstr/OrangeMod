package com.athomas.restwsnonjpa.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "User")
public class User {

	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String location;
	private Integer sub_team_id;
	private int company_name;
	private String position;
	private boolean deleted;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getSub_team_id() {
		return sub_team_id;
	}

	public void setSub_team_id(Integer sub_team_id) {
		this.sub_team_id = sub_team_id;
	}

	public int getCompany_name() {
		return company_name;
	}

	public void setCompany_name(int company_name) {
		this.company_name = company_name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", phone=" + phone + ", location=" + location + ", sub_team_id=" + sub_team_id + ", company_name="
				+ company_name + ", position=" + position + ", deleted=" + deleted + "]";
	}
}
